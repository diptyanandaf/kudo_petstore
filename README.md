Kudo-Test-Petstore

1. Install sqldriver & gin framework.
	$ go get "github.com/go-sql-driver/mysql"
	$ go get "github.com/gin-gonic/gin"
2. Create database 'petstore' on MySQL.
3. Run dbscript.go to migrate the table structure.
4. Run petstore.go
5. Use Postman for API testing.

Regards,