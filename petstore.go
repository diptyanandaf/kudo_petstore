package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/petstore")
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		fmt.Print(err.Error())
	}
	type Pet_obj struct {
		Id    int
		Name  string
		Age   int
		Photo string
	}
	method := gin.Default()

	//POST insert new pet without photo
	method.POST("/pet", func(c *gin.Context) {
		id := c.PostForm("id")
		name := c.PostForm("name")
		age := c.PostForm("age")
		stmt, err := db.Prepare("insert into pet_obj (id, name, age, photo) values(?, ?, ?, 'not available');")
		if err != nil {
			fmt.Print(err.Error())
		}
		_, err = stmt.Exec(id, name, age)

		if err != nil {
			fmt.Print(err.Error())
		}

		defer stmt.Close()

		//Message
		c.JSON(http.StatusOK, gin.H{
			"message": fmt.Sprintf(" %s successfully added", name),
		})
	})

	//GET select a pet detail
	method.GET("/pet/:id", func(c *gin.Context) {
		var (
			pet_obj Pet_obj
			result  gin.H
		)
		id := c.Param("id")
		row := db.QueryRow("select id, name, age, photo from pet_obj where id = ?;", id)
		err = row.Scan(&pet_obj.Id, &pet_obj.Name, &pet_obj.Age, &pet_obj.Photo)
		if err != nil {
			//If no results
			result = gin.H{
				"result": nil,
				"count":  0,
			}
		} else {
			result = gin.H{
				"result": pet_obj,
				"count":  1,
			}
		}
		c.JSON(http.StatusOK, result)
	})

	//PUT update a pet details without photo
	method.PUT("/pet/:id", func(c *gin.Context) {
		id := c.Param("id")
		name := c.PostForm("name")
		age := c.PostForm("age")
		stmt, err := db.Prepare("update pet_obj set name= ?, age= ? where id= ?;")
		if err != nil {
			fmt.Print(err.Error())
		}
		_, err = stmt.Exec(name, age, id)
		if err != nil {
			fmt.Print(err.Error())
		}

		defer stmt.Close()

		//Message
		c.JSON(http.StatusOK, gin.H{
			"message": fmt.Sprintf("Successfully update to %s, %s years old", name, age),
		})
	})

	//DELETE delete a pet data
	method.DELETE("/pet/:id", func(c *gin.Context) {
		id := c.Param("id")
		stmt, err := db.Prepare("delete from pet_obj where id= ?;")
		if err != nil {
			fmt.Print(err.Error())
		}
		_, err = stmt.Exec(id)
		if err != nil {
			fmt.Print(err.Error())
		}

		//Message
		c.JSON(http.StatusOK, gin.H{
			"message": fmt.Sprintf("Successfully delete pet ID: %s", id),
		})
	})
	method.Run(":3000")

	//POST upload photo
}
